// #0. IMPORTS
import {writable} from 'svelte/store';

// #1. EXAMPLE OF STORE CREATION
export const ExampleStore = writable (0);

// #2. HOW TO USE THE STORE (.SVELTE FILE)

// // #2.1. IMPORT THE STORE
// // import {ExampleStore} from 'src\stores\ExampleStore.js';

// // #2.2. SUBSCRIBE TO THE STORE
// // const ExampleStoreUnSub = ExampleStore.subscribe (value => {
// //     # TODO : do something here
// // });

// // #2.3. MAKE SURE YOU UNSUBSCRIBE ON DESTROY TO PREVENT MEMORY LEAKS
// // onDestroy (() => {
// //     ExampleStoreUnSub ();
// // });