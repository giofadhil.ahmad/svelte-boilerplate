export const AuthHelpers = {
  isUserLoggedIn: () => {
    let isUserLoggedIn = false;

    if (
      localStorage.getItem ('user') &&
      JSON.parse (localStorage.getItem ('user')).app == 'OurApp'
    ) {
      isUserLoggedIn = true;
    }

    return isUserLoggedIn;
  },
};
